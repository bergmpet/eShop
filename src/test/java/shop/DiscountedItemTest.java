package shop;

import cz.cvut.eshop.shop.DiscountedItem;
import org.junit.Test;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DiscountedItemTest {
    @Test
    public void setDiscountFrom_correctDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        String newDateString = "15.04.2005";

        discountedItem.setDiscountFrom(newDateString);
        Date date = discountedItem.getDiscountFrom();

        assertEquals(15, date.getDate());
        assertEquals(3, date.getMonth());
        assertEquals(105, date.getYear());
    }

    @Test
    public void setDiscountFrom_wrongDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        String newDateString = "abc";

        discountedItem.setDiscountFrom(newDateString);
        Date date = discountedItem.getDiscountFrom();

        assertEquals(10, date.getDate());
        assertEquals(4, date.getMonth());
        assertEquals(117, date.getYear());
    }

    @Test
    public void setDiscountTo_correctDateFormatString() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        String newDateString = "15.04.2020";

        discountedItem.setDiscountTo(newDateString);
        Date date = discountedItem.getDiscountTo();

        assertEquals(15, date.getDate());
        assertEquals(3, date.getMonth());
        assertEquals(120, date.getYear());
    }

    @Test
    public void setDiscountTo_wrongDateFormatString() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        String newDateString = "abcdefs";

        discountedItem.setDiscountTo(newDateString);
        Date date = discountedItem.getDiscountTo();

        assertEquals(10, date.getDate());
        assertEquals(4, date.getMonth());
        assertEquals(118, date.getYear());
    }

    @Test
    public void getDiscountedPrice_zeroDiscount() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",0, "10.05.2017", "10.05.2018");

        float value = discountedItem.getDiscountedPrice();

        assertEquals(1.6f, value, 0.001f);
    }

    @Test
    public void getDiscountedPrice_nozeroDiscount() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");

        float value = discountedItem.getDiscountedPrice();

        assertEquals(0.64f, value, 0.001f);
    }

    @Test
    public void parseDate_correctDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "abcd", "10.05.2018");

        Date date = discountedItem.getDiscountFrom();

        assertNull(date);
    }

    @Test
    public void parseDate_wrongDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");

        Date date = discountedItem.getDiscountFrom();

        assertEquals(10, date.getDate());
        assertEquals(4, date.getMonth());
        assertEquals(117, date.getYear());
    }

    @Test
    public void setDiscountTo_correctDateFormatDate() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        Date newDate = new Date(120,3,15);

        discountedItem.setDiscountTo(newDate);
        Date date = discountedItem.getDiscountTo();

        assertEquals(15, date.getDate());
        assertEquals(3, date.getMonth());
        assertEquals(120, date.getYear());
    }

    @Test
    public void setDiscountTo_nullDateFormatDate() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        Date newDate = null;

        discountedItem.setDiscountTo(newDate);
        Date date = discountedItem.getDiscountTo();

        assertNull(date);
    }

    @Test
    public void getDiscountedPrice_reflectsChangeOfDiscount() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        int newDiscount = 40;

        discountedItem.setDiscount(newDiscount);
        int actual = discountedItem.getDiscount();
        float value = discountedItem.getDiscountedPrice();

        assertEquals(0.96f, value, 0.001f);
        assertEquals(newDiscount, actual);
    }

    @Test
    public void getOriginalPrice_priceIsCorrect() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");

        float actual = discountedItem.getOriginalPrice();

        assertEquals(1.6f, actual, 0.001f);
    }
}
