package shop;

import cz.cvut.eshop.shop.DiscountedItem;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ShoppingCartTest {
    @Test
    public void removeItem_itemDoesNotExist() {
        ShoppingCart shoppingCart = new ShoppingCart();
        Item item1 = new StandardItem(1, "name", 0.28f, "category", 1);
        Item item2 = new DiscountedItem(2, "name2", 2.22f, "category2", 50, new Date(1996,12,20), new Date(2020,11,15));
        shoppingCart.addItem(item1);
        shoppingCart.addItem(item2);

        shoppingCart.removeItem(5);
        int items = shoppingCart.getItemsCount();

        assertEquals(2, items);
    }

    @Test
    public void removeItem_itemExists() {
        ShoppingCart shoppingCart = new ShoppingCart();
        Item item = new StandardItem(1, "name", 0.28f, "category", 1);
        shoppingCart.addItem(item);

        shoppingCart.removeItem(1);
        int items = shoppingCart.getItemsCount();

        assertEquals(0, items);
    }

    @Test
    public void getTotalPrice_StandardItem() {
        Item item1 = new StandardItem(2, "name1", 1.28f, "category1", 1);
        Item item2 = new StandardItem(1, "name2", 2.01f, "category2", 2);
        ArrayList<Item> list = new ArrayList<>();
        list.add(item1);
        list.add(item2);
        ShoppingCart shoppingCart = new ShoppingCart(list);

        float price = shoppingCart.getTotalPrice();

        assertEquals(3.29f, price,0.01);
    }

    @Test
    public void getTotalPrice_DiscountedItem() {
        Item item1 = new DiscountedItem(2, "name1", 1.6f, "category1",60, "10.05.2017", "10.05.2018");
        Item item2 = new DiscountedItem(1, "name2", 2.22f, "category2", 50, new Date(1996,12,20), new Date(2020,11,15));
        ArrayList<Item> list = new ArrayList<>();
        list.add(item1);
        list.add(item2);
        ShoppingCart shoppingCart = new ShoppingCart(list);

        float price = shoppingCart.getTotalPrice();

        assertEquals(1.75f, price,0.01);
    }

    @Test
    public void getCartItems_itemIsNull()
    {
        ShoppingCart shoppingCart = new ShoppingCart(null);

        ArrayList<Item> items = shoppingCart.getCartItems();

        assertNull(items);
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void addItem_itemIsNull()
    {
        ShoppingCart shoppingCart = new ShoppingCart(null);
        Item item = new StandardItem(1, "name", 0.28f, "category", 1);

        shoppingCart.addItem(item);
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void removeItem_itemIsNull()
    {
        ShoppingCart shoppingCart = new ShoppingCart(null);

        shoppingCart.removeItem(2);
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void itemsCount_itemIsNull()
    {
        ShoppingCart shoppingCart = new ShoppingCart(null);

        shoppingCart.getItemsCount();
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void getTotalPrice_itemIsNull()
    {
        ShoppingCart shoppingCart = new ShoppingCart(null);

        shoppingCart.getItemsCount();
    }
}
